set Name LaunchRuns
set path [file normalize "[file dirname [info script]]/.."]
if { $::argc eq 0 } {
    puts "USAGE: $::argv0 <project> \[output directory\] \[number of jobs\]"
    exit 1
} else {
    set project [lindex $argv 0]
    if { $::argc > 1 } {
	set main_folder [file normalize [lindex $argv 1]]
    } else {
	set main_folder [file normalize "$path/../../VivadoProject/$project/$project.runs/"]
    }

    if { $::argc > 2 } {
	set NJOBS [lindex $argv 2]
    } else {
	set NJOBS 4
    }

    if { $::argc > 3 } {
	set no_time [lindex $argv 3]
    } else {
	set no_time 0
    }
}

set old_path [pwd]
cd $path
source ./hog.tcl
Msg Info "Number of jobs set to $NJOBS."

set commit [GetHash ALL ../../]

Msg Info "Running project script: $project.tcl..."
source -notrace ../../Top/$project/$project.tcl
Msg Info "Upgrading IPs if any..."
set ips [get_ips *]
if {$ips != ""} {
    upgrade_ip $ips
}
Msg Info "Creating directory and buypass file..."
file mkdir $main_folder
set cfile [open $main_folder/buypass_commit w]
puts $cfile $commit
close $cfile
if {$no_time == 1 } {
    set cfile [open $main_folder/no_time w]
    puts $cfile $commit
    close $cfile
}
Msg Info "Starting complete design flow..."
launch_runs impl_1 -to_step write_bitstream -jobs $NJOBS -dir $main_folder
cd $old_path
